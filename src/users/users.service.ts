import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email
    user.fullName = createUserDto.fullName
    user.gender = createUserDto.gender
    user.password = createUserDto.password
    user.roles = JSON.parse(createUserDto.roles)
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    return this.usersRepository.save(user);

  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
  
  const updateUser = await this.usersRepository.findOneOrFail({
    where: { id },
    relations: { roles: true },
  });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;

    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex((role) => role.id === updateUser.roles[i].id,);
      if (index < 0) {
        updateUser.roles.splice(i, 1)
      }
    }
    await this.usersRepository.save(updateUser);

    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },

}
